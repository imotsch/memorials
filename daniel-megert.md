# In Memory of Daniel Megert



Please add your contribution below via a merge request. 

## Denis Roy

Daniel was passionate about all things Eclipse, and worked tirelessly to make things better. Easily approachable and personable, he would engage constructively to challenge status quo and was very understanding of the constraints that were slowing progress. 

## Jonah Graham

Thank you to Dani for your tireless work on Eclipse. Your absence will be sorely missed. You leave behind an excellent legacy of a mature and stable Eclipse project that you dedicated so many years of your life contributing to and improving.

## Mickael Istria

You were not only a great technical leader who always managed to drive the community towards the best solutions; but more importantly  an open-minded, listening, trusting and sharing individual who acted as a mentor for a lot of contributors -including me- and made them better in all the way software development can need. You've been the project lead who has allowed the Eclipse Platform and JDT project to become more active, more diverse, more exciting and more relevant in the most difficult time; most of the good things that are happening today are the result of your efforts combined with your very pleasant personality. Thanks eternally for all that!
